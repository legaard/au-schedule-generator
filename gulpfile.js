var gulp = require('gulp'),
    concat = require('gulp-concat'),
    uglify = require('gulp-uglify'),
    less = require('gulp-less'),
    cleanCSS = require('gulp-clean-css'),
    clean = require('gulp-clean'),
    sourcemaps = require('gulp-sourcemaps'),
    browserSync = require('browser-sync').create();

gulp.task('default', ['js', 'less']);

gulp.task('js', function(){
  return gulp.src('./app/**/*.js')
    .pipe(concat('app.min.js'))
    .pipe(uglify())
    .pipe(gulp.dest('./build'));
});

gulp.task('less', function(){
  return gulp.src('./assets/less/*.less')
    .pipe(sourcemaps.init())
    .pipe(less())
    .pipe(cleanCSS({compatibility: 'ie8'}))
    .pipe(sourcemaps.write('./../maps'))
    .pipe(gulp.dest('./build'));
});

gulp.task('clean', function(){
  return gulp.src(['./build', './maps'], {read: false})
    .pipe(clean());
});

gulp.task('browser-sync', function(){
  browserSync.init({
    server: {
      baseDir: './'
    }
  });
});

gulp.task('watch', ['browser-sync', 'js', 'less'], function(){
  return gulp.watch(['./**/*.html', './app/**/*.js', 'assets/less/*.less'],
                    ['js', 'less', browserSync.reload]);
});
