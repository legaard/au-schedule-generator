# AU Schedule Generator
This project sets out to make life easier for students, at the Science and Technology department at Aarhus University (AU), by offering a simple way to generate schedules for one person or multiple people (e.g. a study group). Using this generator the student can:

- Generate a weekly schedule for classes at the current semester (two quarters).
- Add multiple people to the same schedule and easily toggle between the various students.
- Filter all classes shown; e.g. name of the course, day or type of class (lecture, exercises, etc.).

*Note: The data used in this project is retrieved from [timetable.scitech.au.dk](http://timetable.scitech.au.dk/apps/skema/VaelgElevskema.asp?webnavn=skema) – always make sure the generated schedule is correct using this website.*

## Developers
The following people are developing on the AU Schedule Generator:

- Lasse Legaard – AngularJS (client-side) and Node.JS (server-side)
- Jonas Techen – design and HTML styling (client-side)

## Technologies
Currently, this project is based on AngularJS 1.5.8, but will eventually be upgraded to Angular 2.0. The data used in the project is provided by a Node.js application (running on a DigitalOcean droplet) which scrapes an official AU website. The Node.js application is running on [au-service.legaard.xyz](http://au-service.legaard.xyz).


## How work on the project
First clone/fork the project, then navigate to the project folder and run the following command to install all project related dependencies (*npm* must be installed):
```
npm install
```

In order to run all the build scripts, Gulp must also be installed globally. Run the command:
```
npm install -g gulp-cli
```

Once the installation has finished the various Gulp commands can be executed.

### Gulp
As mentioned in the last section Gulp is used as a build system.
To run a live server (browser-sync) for development run the command:

```
gulp watch
```

To clean the *build* folder run the command:
```
gulp clean
```
