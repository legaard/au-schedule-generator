angular.module('schedule')

.directive('filter', function(){
  return {
    restrict: 'E',
    replace: true,
    scope: {},
    controller: 'FilterCtrl',
    templateUrl: 'app/directives/filter.html'
  };
});
