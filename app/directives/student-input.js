angular.module('schedule')

.directive('studentInput', function(){
  return {
    restrict: 'E',
    replace: true,
    scope: {},
    controller: 'StudentCtrl',
    templateUrl: 'app/directives/student-input.html',
  };
});
