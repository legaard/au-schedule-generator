angular.module('schedule')

.directive('scheduleColumn', function(){
  return {
    restrict: 'E',
    replace: true,
    scope: {
      day: '@',
      filter: '=',
      courses: '='
    },
    controller: 'ScheduleCtrl',
    templateUrl: 'app/directives/schedule-column.html'
  };
});
