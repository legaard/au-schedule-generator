angular.module('schedule')

.directive('feedback', function(){
  return {
    restrict: 'E',
    replace: true,
    scope: {},
    controller: 'FeedbackCtrl',
    templateUrl: 'app/directives/feedback.html'
  };
});
