angular.module('schedule')

.service('ScheduleService', ['$http', '$q',
                              'UtilService', 'Config',
                             function($http, $q,
                                      UtilService, Config){

  var keyPrefix = 'schedule';

  this.getSchedule = function(studentId) {
    var deferred = $q.defer();

    //Check if the id is test person
    var url = studentId == Config.TEST_PERSON_NUMBER ?
              Config.DATA_URL + ':' + Config.DATA_URL_PORT + '/test?data=schedule&callback=JSON_CALLBACK' :
              Config.DATA_URL + ':' + Config.DATA_URL_PORT + '/schedule?studentId=' + studentId + '&callback=JSON_CALLBACK';

    $http.jsonp(url)
    .then(function(response) {
      //Add student id to data and format it
      var data = response.data;
      data.studentId = studentId;
      var formattedData = formatData(data);

      deferred.resolve(formattedData);
    }, function(error) {
      deferred.reject('Failed to retrieve data from the server');
    });

    return deferred.promise;
  };

  function formatData(data) {
    var newData = {};
    newData.studentName = data.studentName;
    newData.weeks = extractWeeks(data);
    //newData.weeks = mergeWeeks(newData.weeks);
    return newData;
  }

  //Extract weeks from courses and create object from that
  function extractWeeks(data){
    var newWeeks = {};
    var studentName = data.studentName;
    var courses = data.courses;

    //HELPER METHODS
    function addWeekAndCourse(week, course){
      if(!newWeeks[week]){
        newWeeks[week] = {};
        newWeeks[week].monday = [];
        newWeeks[week].tuesday = [];
        newWeeks[week].wednesday = [];
        newWeeks[week].thursday = [];
        newWeeks[week].friday = [];
      }

      //Add extra details to the course
      course = addTimeSpan(course);
      course.studentInitials = UtilService.extractInitials(studentName);
      course.studentId = data.studentId;

      switch (course.day) {
        case 'Mandag':
          newWeeks[week].monday.push(course);
          break;
        case 'Tirsdag':
          newWeeks[week].tuesday.push(course);
          break;
        case 'Onsdag':
          newWeeks[week].wednesday.push(course);
          break;
        case 'Torsdag':
          newWeeks[week].thursday.push(course);
          break;
        case 'Fredag':
          newWeeks[week].friday.push(course);
          break;
        default:
          break;
      }
    }

    function addTimeSpan(course){
      var timeArr = course.time.split('-');
      var timeLength = parseInt(timeArr[1]) - parseInt(timeArr[0]);
          startTime = parseInt(timeArr[0]);

      course.startTime = startTime;
      course.timeLength = timeLength;
      return course;
    }

    function contains(obj, con){
      return obj.indexOf(con) !== -1;
    }

    //Sort courses by time of the day
    courses.sort(function(a, b){
      return a.time.split('-')[0] - b.time.split('-')[0];
    });

    //Extract all weeks and create courses for each week
    for (var i = 0; i < courses.length; i++) {
      var course = courses[i];
      var weeks = course.weeks;
      var isCommaSeparted = contains(weeks, ',');
      var hasHyphen = contains(weeks, '-');

      if(isCommaSeparted){
        var intervalList = weeks.split(',');

        for (var j = 0; j < intervalList.length; j++) {
          var commaWeeks = intervalList[j];
          var isInterval = contains(commaWeeks, '-');

          if(isInterval){
            var commaStart = parseInt(commaWeeks.split('-')[0]);
            var commaEnd = parseInt(commaWeeks.split('-')[1]);

            for (var l = commaStart; l <= commaEnd; l++) {
              addWeekAndCourse(l, course);
            }

          } else {
            addWeekAndCourse(parseInt(commaWeeks), course);
          }
        }

      } else if (hasHyphen) {
        var hyphStart = parseInt(weeks.split('-')[0]);
        var hyphEnd = parseInt(weeks.split('-')[1]);

        for (var k = hyphStart; k <= hyphEnd; k++) {
          addWeekAndCourse(k, course);
        }

      } else {
        addWeekAndCourse(parseInt(weeks), course);
      }
    }

    return newWeeks;
  }

  //Function used to merge weeks for a (new) data object
  function mergeWeeks(data){
    var mergedWeeks = {};

    //Create (sorted) array of keys which can be used for iteration
    var weekKeys = Object.keys(data).sort(function(a, b){
      return a.split('-')[0] - b.split('-')[0];
    });

    var start = weekKeys[0];
    var size = 0;

    for (var i = 1; i <= weekKeys.length; i++) {
      var toCompare = weekKeys[i];

      if(angular.equals(data[start], data[toCompare])){
        size++;
      } else {
        var end = parseInt(start) + size;
        var newWeekKey = start == end ? start : start + '-' + end;
        mergedWeeks[newWeekKey] = data[start];

        //Reset size and select new start
        size = 0;
        start = weekKeys[i];
      }
    }

    return mergedWeeks;
  }

}]);
