angular.module('schedule')

.service('ScrollService', ['$location', 'DateService',
                           function($location, DateService){

  this.updatePagePosition = function(){
    var weekNumber = DateService.getCurrentWeek();
    $location.hash('uge' + weekNumber);
  };
}]);
