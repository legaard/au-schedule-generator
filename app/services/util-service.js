angular.module('schedule')

.service('UtilService', function(){
  this.extractInitials = function(name){
    var nameArr = name.split(' ');

    //Extract first character from firstname and surname, then capitalize it
    var firstInitial = nameArr[0].substring(0, 1).toUpperCase();
    var secondInital = nameArr[nameArr.length - 1].substring(0,1).toUpperCase();

    return firstInitial + secondInital;
  };
});
