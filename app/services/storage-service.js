angular.module('schedule')

.service('StorageService', ['$window', '$log', 'Config', function($window, $log, Config){

  var hasStorage = angular.isDefined($window.localStorage);

  //Check if there is storage avaible in the given browser
  if (!hasStorage) {
    $log.warn(Config.APPLICATION_NAME, '==>', 'No storage available in this browser');
  }

  this.saveData = function(key, data){
    if(hasStorage){
      $window.localStorage.setItem(key, angular.toJson(data));
    }
  };

  this.getData = function(key){
    if(hasStorage){
      return angular.fromJson($window.localStorage.getItem(key));
    }
  };

  this.removeData = function(key){
    if (hasStorage) {
      $window.localStorage.removeItem(key);
    }
  };

  this.clearData = function(){
    if(hasStorage){
      $window.localStorage.clear();
    }
  };

}]);
