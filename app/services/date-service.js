angular.module('schedule')

.service('DateService', function(){
  var weekday = [];
  weekday.push('sunday');
  weekday.push('monday');
  weekday.push('tuesday');
  weekday.push('wednesday');
  weekday.push('thursday');
  weekday.push('friday');
  weekday.push('saturday');

  this.getCurrentDay = function(){
    var day = new Date();
    return weekday[day.getDay()];
  };

  this.getCurrentWeek = function(){
    //Credit: User RobG from stackoverflow
    var day = new Date();
    day.setHours(0, 0, 0);
    day.setDate(day.getDate() + 4 - (day.getDay() || 7));
    return Math.ceil((((day - new Date(day.getFullYear(), 0 , 1)) / 8.64e7) + 1) /7);
  };
});
