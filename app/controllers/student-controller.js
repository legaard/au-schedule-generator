angular.module('schedule')

.controller('StudentCtrl', ['$scope', '$log', '$timeout', 'UtilService',
                            'ScheduleService','StorageService',
                            'Config', 'Events', 'ColorFactory', 'ScrollService',
                            function($scope, $log, $timeout, UtilService,
                                     ScheduleService, StorageService,
                                     Config, Events, ColorFactory, ScrollService) {

  var STORAGE_PREFIX = 'studentsArray';
  var requestInProgress = false;
  $scope.students = StorageService.getData(STORAGE_PREFIX) === null ? [] :
                      StorageService.getData(STORAGE_PREFIX);

  $scope.addStudent = function(studentId){
    //If studentId is empty --> return
    if (!studentId) return;

    //If a request is being progressed --> return
    if(requestInProgress) return;

    //Do not add a student if max has been reached --> return
    if($scope.students.length >= Config.MAX_NUMBER_OF_STUDENTS) {
      $scope.$emit(Events.START_LOADING, {
        time: 5000,
        message: 'Du kan max tilføje ' + Config.MAX_NUMBER_OF_STUDENTS + ' studerende'
      });

      return;
    }

    //If student already exist --> notify user and return
    if (doesStudentExist(studentId)) {
      $scope.$emit(Events.START_LOADING, {
        time: 5000,
        message: 'Studerende allerede tilføjet'
      });

      return;
    }

    //Start loading indicator and set request in progress
    $scope.$emit(Events.START_LOADING, {spinner: true});
    requestInProgress = true;

    //Get requested student
    ScheduleService.getSchedule(studentId)
      .then(function(data){
        //Generate color
        var color = ColorFactory.generateColor();

        //Extract initials from name
        var initials = UtilService.extractInitials(data.studentName);

        var setChecked = hasMaxNumberOfStudentsChecked() ? false : true;

        //Create new student, add to array and save to storage
        var newStudent = {name: data.studentName,
                          id: studentId,
                          initials: initials,
                          checked: setChecked,
                          color: color};
        $scope.students.unshift(newStudent);
        StorageService.saveData(STORAGE_PREFIX, $scope.students);

        //Clear input field
        $scope.studentId = '';

        //Emit events (added and stop spinner)
        var eventData = {};
        eventData.student = newStudent;
        eventData.data = data;
        $scope.$emit(Events.STUDENT_ADDED, eventData);
        $scope.$emit(Events.STOP_LOADING, undefined);

      }, function(err) {
        $log.error(Config.APPLICATION_NAME, '==>', err);
        $scope.$emit(Events.START_LOADING, {
          time: 5000,
          message: 'Studerende kunne ikke findes'
        });
      })
      .finally(function(){
        requestInProgress = false;
      });
  };

  $scope.removeStudent = function(studentId){

    $scope.students.forEach(function(student, index){
      if (student.id === studentId) {
        //Remove student
        $scope.students.splice(index, 1);

        //Release color
        ColorFactory.releaseColor(student.color);

        //Remove from array and save to local storage
        StorageService.saveData(STORAGE_PREFIX, $scope.students);

        //Emit event
        $scope.$emit(Events.STUDENT_REMOVED, student);
      }
    });
  };

  $scope.removeAllStudents = function(){
    $scope.students = [];
    StorageService.removeData(STORAGE_PREFIX);
  };

  $scope.setChecked = function(checked){

    //Make sure that only four students can be checked at once
    if (hasMaxNumberOfStudentsChecked() && !this.student.checked) {
      $scope.$emit(Events.START_LOADING, {
        time: 5000,
        message: 'Der kan maks. vises ' +
                  Config.MAX_NUMBER_OF_STUDENTS_CHECKED +
                  ' studerende'
      });
      return;
    }

    //Get the index of the student
    var studentIndex = $scope.students.indexOf(this.student);

    if(studentIndex > -1){
      var changedStudent = $scope.students[studentIndex];

      changedStudent.checked = checked;
      $scope.students[studentIndex] = changedStudent;
      StorageService.saveData(STORAGE_PREFIX, $scope.students);

      //Emit event
      $scope.$emit(Events.STUDENT_CHANGED, changedStudent);
    }

  };

  $scope.enterKey = function($event, studentId){
    var keyCode = $event.which || $event.keyCode;

    if (keyCode == 13) {
      $scope.addStudent($scope.studentId);
    }
  };

  function doesStudentExist(studentId){
    for (var i = 0; i < $scope.students.length; i++) {
      if ($scope.students[i].id == studentId) return true;
    }
    return false;
  }

  function hasMaxNumberOfStudentsChecked(){
    var numberOfCheckedStudents = 0;

    $scope.students.forEach(function(obj){
      if(obj.checked) numberOfCheckedStudents++;
    });

    return numberOfCheckedStudents >= Config.MAX_NUMBER_OF_STUDENTS_CHECKED;
  }

  //Set url to current week
  ScrollService.updatePagePosition();

  //Function that fetches data on start up
  $timeout(function(){
    var numberOfRequestsLeft = $scope.students.length;
    var eventDataArr = [];

    //If there is no students --> return
    if($scope.students.length === 0 || $scope.students === null) return;

    //Start loading indicator
    $scope.$emit(Events.START_LOADING, {spinner: true});

    $scope.students.forEach(function(obj, index){

      ScheduleService.getSchedule(obj.id)
        .then(function(data){
          //Emit event
          var eventData = {};
          eventData.student = obj;
          eventData.data = data;
          eventDataArr.push(eventData);

          numberOfRequestsLeft--;

          if (numberOfRequestsLeft === 0) {
              $scope.$emit(Events.STUDENT_ADDED, eventDataArr);
              $scope.$emit(Events.STOP_LOADING, undefined);
          }
        }, function(err) {
          $log.error(Config.APPLICATION_NAME, '==>', err);
          $scope.$emit(Events.START_LOADING, {
            time: 5000,
            message: 'Studerende kunne ikke hentes fra server'
          });
        });
    });
  }, 10);

}]);
