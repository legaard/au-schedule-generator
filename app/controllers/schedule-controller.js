angular.module('schedule')

.controller('ScheduleCtrl', ['$scope', '$anchorScroll', '$window',
                             '$timeout','Events', 'DateService', 'Config',
                              function ($scope, $anchorScroll, $window,
                                        $timeout, Events, DateService, Config) {

  var students = [];
  var courseData = [];
  $scope.weeks = [];
  $scope.filterValue = '';
  $scope.shouldBeOpen = false;

  $scope.$on(Events.ADD_SCHEDULE, function(event, args){
    if (angular.isArray(args)) {
      //Add all students on startup
      args.forEach(function(obj){
        students.push(obj.student);
        courseData.push(obj.data);
        //Scroll to the current week on startup
        $timeout(function(){
          $anchorScroll();
          $window.scrollBy(0, -125);
        }, 100);
      });

    } else {
      students.push(args.student);
      courseData.push(args.data);
    }
    updateScheduleData();
  });

  $scope.$on(Events.REMOVE_SCHEDULE, function(event, args){
    var index = students.indexOf(args);

    if (index > -1){
      students.splice(index, 1);
      courseData.splice(index, 1);
      updateScheduleData();
    }
  });

  $scope.$on(Events.UPDATE_SCHEDULE, function(event, args){
    var updatedStudent = args;

    students.forEach(function(obj, index){
      if (obj.id == updatedStudent.id){
        students[index] = updatedStudent;
        updateScheduleData();
      }
    });
  });

  $scope.$on(Events.FILTER_SCHEDULE, function(event, args){
    $scope.filterValue = args;
  });

  $scope.getCourseColor = function(studentId){
    var color;

    students.forEach(function(obj){
      if(obj.id == studentId) color = obj.color.name;
    });

    return color;
  };

  $scope.isCurrentDay = function(day, week){
    return DateService.getCurrentDay() == day &&
            DateService.getCurrentWeek() == week;
  };

  $scope.isCurrentWeek = function(week){
    return week == DateService.getCurrentWeek();
  };

  $scope.getTestPersonId = function(){
    return Config.TEST_PERSON_NUMBER;
  };

  function updateCheckedStudents(){
    var checkedStudents = 0;

    students.forEach(function(obj, index){
      if (obj.checked && Object.keys(courseData[index].weeks).length !== 0){
        checkedStudents++;
      }
    });

    $scope.studentsChecked = checkedStudents === 1 ?
                                checkedStudents + 1 : checkedStudents;
    $scope.shouldBeOpen = checkedStudents === 1;
  }

  $scope.numberOfCheckedStudents = function(){
    var numOfStudentsChecked = 0;

    students.forEach(function(obj){
      if(obj.checked) numOfStudentsChecked++;
    });

    return numOfStudentsChecked;
  };

  /* Function called everytime students are changed */
  function updateScheduleData(){
    var combinedWeeks = {};
    var weeks = [];

    students.forEach(function(obj, index){
      if (obj.checked){
        addWeeks(courseData[index].weeks);
      }
    });

    //Convert combinedWeeks to array and sort it
    weeks = Object.keys(combinedWeeks)
                        .map(function(key) {
                          var newObj = combinedWeeks[key];
                          newObj.weekNumber = key;
                          return newObj;
                        })
                        .sort(function(a, b){
                          return parseInt(a.weekNumber) - parseInt(b.weekNumber);
                        });

    //Sort function – used by the addWeeks function
    function sortByStartTime(a, b){
      return a.startTime - b.startTime;
    }

    //Add weeks to combinedWeeks – used when iterate data
    function addWeeks(weeks){
      Object.keys(weeks).forEach(function(key){
        //If the week does not exist, then create it
        if (!combinedWeeks[key]) {
          combinedWeeks[key] = {};
          combinedWeeks[key].monday = [];
          combinedWeeks[key].tuesday = [];
          combinedWeeks[key].wednesday = [];
          combinedWeeks[key].thursday = [];
          combinedWeeks[key].friday = [];
        }

        //Merge data into the (new) day arrays
        combinedWeeks[key].monday = combinedWeeks[key].monday.concat(weeks[key].monday);
        combinedWeeks[key].tuesday = combinedWeeks[key].tuesday.concat(weeks[key].tuesday);
        combinedWeeks[key].wednesday = combinedWeeks[key].wednesday.concat(weeks[key].wednesday);
        combinedWeeks[key].thursday = combinedWeeks[key].thursday.concat(weeks[key].thursday);
        combinedWeeks[key].friday = combinedWeeks[key].friday.concat(weeks[key].friday);

        //Sort the day arrays based on time
        combinedWeeks[key].monday.sort(sortByStartTime);
        combinedWeeks[key].tuesday.sort(sortByStartTime);
        combinedWeeks[key].wednesday.sort(sortByStartTime);
        combinedWeeks[key].thursday.sort(sortByStartTime);
        combinedWeeks[key].friday.sort(sortByStartTime);
      });
    }
    //Assign the new data to the $scope
    $scope.weeks = weeks;

    //Update open boolean
    updateCheckedStudents();
  }
}]);
