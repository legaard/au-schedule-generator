angular.module('schedule')

.controller('FilterCtrl', ['$scope', '$anchorScroll', '$window',
                           'Events', 'DateService',
                           function($scope, $anchorScroll, $window,
                                    Events, DateService) {

  $scope.$watch('filter', function(newValue){
    $scope.$emit(Events.FILTER_CHANGED, newValue);
  });

  $scope.currentWeek = DateService.getCurrentWeek();

  $scope.goToCurrentWeek = function(){
    $anchorScroll();
    $window.scrollBy(0, -125);
  };
}]);
