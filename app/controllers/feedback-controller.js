angular.module('schedule')

.controller('FeedbackCtrl', ['$scope', '$timeout', 'Events',
                            function($scope, $timeout, Events) {

  $scope.message = '';
  $scope.showSpinner = false;
  var isActive = false;

  $scope.$on(Events.START_LOADING_B, function(event, args) {
    if (isActive) {
      return;
    }

    $scope.showSpinner = args.spinner === true ? true : false;
    $scope.message = args.message ? args.message : '';
    if(args.time) hideFeedbackIn(args.time);
  });

  $scope.$on(Events.STOP_LOADING_B, function(){
    $scope.showSpinner = false;
    $scope.message = '';
  });

  function hideFeedbackIn(millis){
    isActive = true;

    $timeout(function() {
      $scope.message = '';
      $scope.showSpinner = false;
      isActive = false;
    }, millis);
  }
}]);
