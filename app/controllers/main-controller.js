angular.module('schedule')

.controller('MainCtrl', ['$scope', 'Events', function($scope, Events){
  $scope.$on(Events.STUDENT_ADDED, function(event, args){
    $scope.$broadcast(Events.ADD_SCHEDULE, args);
  });

  $scope.$on(Events.STUDENT_REMOVED, function(event, args){
    $scope.$broadcast(Events.REMOVE_SCHEDULE, args);
  });

  $scope.$on(Events.STUDENT_CHANGED, function(event, args){
    $scope.$broadcast(Events.UPDATE_SCHEDULE, args);
  });

  $scope.$on(Events.FILTER_CHANGED, function(event, args){
    $scope.$broadcast(Events.FILTER_SCHEDULE, args);
  });

  $scope.$on(Events.START_LOADING, function(event, args){
    $scope.$broadcast(Events.START_LOADING_B, args);
  });

  $scope.$on(Events.STOP_LOADING, function(event, args){
    $scope.$broadcast(Events.STOP_LOADING_B, args);
  });

}]);
