// Instantiating and configuring the module
angular.module('schedule', ['ngRoute'])

.config(['$routeProvider', 'Config', function($routeProvider, Config){

  //Log relevant application information to the console on start
  console.info(Config.APPLICATION_NAME, '==>',
  'Created by', Config.DEVELOPERS[0].name, 'and', Config.DEVELOPERS[1].name);

  console.info(Config.APPLICATION_NAME, '==>',
  'Web application uses AngularJS version', angular.version.full);

  console.info(Config.APPLICATION_NAME, '==>',
  'Max number of students is set to', Config.MAX_NUMBER_OF_STUDENTS);

  console.info(Config.APPLICATION_NAME, '==>',
  'Max number of students to be checked is set to', Config.MAX_NUMBER_OF_STUDENTS_CHECKED);

  //Setup routes
  $routeProvider
  .when('/skema', {
    templateUrl: 'app/views/schedule.html',
    controller: 'ScheduleCtrl'
  })
  .otherwise({
    redirectTo: '/skema'
  });
}]);
