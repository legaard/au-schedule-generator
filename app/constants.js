angular.module('schedule')

.constant('Config', {
  APPLICATION_NAME: 'AU Schedule Generator',
  MAX_NUMBER_OF_STUDENTS: 10,
  MAX_NUMBER_OF_STUDENTS_CHECKED: 4,
  TEST_PERSON_NUMBER: 23061912,
  DEVELOPERS: [
    {
      name: 'Lasse Legaard',
      linkedIn: 'https://linkedin.com/in/lasselegaard',
      portfolio: 'http://legaard.xyz'
    },
    {
      name: 'Jonas Techen',
      linkedIn: 'https://linkedin.com/in/jonastechen',
      portfolio:'http://jonastechen.dk'
    }
  ],
  SCRAPING_URL: 'http://timetable.scitech.au.dk/apps/skema/VaelgelevSkema.asp?webnavn=skema',
  DATA_URL: 'http://au-service.legaard.xyz',
  DATA_URL_PORT: 2306,
  ISSUE_TRACKER: 'https://bitbucket.org/legaard/au-schedule-generator/issues/new'
})

.constant('Events', {
  STUDENT_ADDED: 'studentAdded',
  STUDENT_REMOVED: 'studentRemoved',
  STUDENT_CHANGED: 'studentChanged',

  ADD_SCHEDULE: 'addSchedule',
  REMOVE_SCHEDULE: 'removeSchedule',
  UPDATE_SCHEDULE: 'updateSchedule',

  FILTER_CHANGED: 'filterChanged',
  FILTER_SCHEDULE: 'filterSchedule',

  START_LOADING: 'startLoading',
  STOP_LOADING: 'stopLoading',

  START_LOADING_B: 'startLoadingBroadcast',
  STOP_LOADING_B: 'stopLoadingBroadcast',

  DISPLAY_MESSAGE: 'displayMessage'
});
