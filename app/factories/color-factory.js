angular.module('schedule')

.factory('ColorFactory', ['StorageService', function(StorageService) {
  var colors = [{name: 'red', hex: '#ca4e4e'},
                {name: 'green', hex: '#60a074'},
                {name: 'blue', hex: '#468dbf'},
                {name: 'yellow', hex: '#e09b08'},
                {name: 'purple', hex: '#bb5297' },
                {name: 'orange', hex: '#efac2a'},
                {name: 'turquoise', hex: '#34c1c6'},
                {name: 'magenta', hex: '#c6366c'},
                {name: 'brown', hex: '#9f7c57'},
                {name: 'lime', hex: '#92bf27'}];

  var STORAGE_PREFIX = 'colorsUsed';

  //Function that returns a color which is availble from the colors array
  function generateColor(){
    var colorsUsed = getColorsUsed();

    for (var i = 0; i < colors.length; i++) {
      var color = colors[i];

      if(colorsUsed.indexOf(color.name) === -1){
        colorsUsed.push(color.name);
        StorageService.saveData(STORAGE_PREFIX, colorsUsed);
        return color;
      }
    }
    return undefined;
  }

  //Function that removes a color from local storage
  function releaseColor(color){
    var colorsUsed = getColorsUsed();

    colorsUsed.forEach(function(element, index){
      if (element == color.name) {
        colorsUsed.splice(index, 1);
        StorageService.saveData(STORAGE_PREFIX, colorsUsed);
      }
    });
  }

  //Function that returns array from the local storage (if it exists)
  function getColorsUsed(){
    return StorageService.getData(STORAGE_PREFIX) === null ? [] :
            StorageService.getData(STORAGE_PREFIX);
  }

  return {generateColor: generateColor, releaseColor: releaseColor};
}]);
