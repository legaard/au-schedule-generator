const express = require('express');
const app = express();
const path = require('path');

const port = process.argv[2] || 80;

app.get('/', (req, res) => {
  res.sendFile(path.join(__dirname + '/index.html'));
});

app.use('/build', express.static('build'));
app.use('/assets', express.static('assets'));
app.use('/app', express.static('app'));

app.listen(port, () => {
  console.log('Application is running on port', port);
});
